import os
import pandas as pd
from datetime import datetime
from websocket import create_connection

def parse_response(response):
    r  = eval(response)
    ticker = r[-1]
    best_bid = float(r[1]['b'][0])
    best_ask = float(r[1]['a'][0])
    return {'ticker' : ticker, 'best_bid' : best_bid, 'best_ask' : best_ask}

if not 'feeds' in os.listdir():
    os.mkdir('feeds')

pairs = ['XBT/EUR','XBT/USD']
ws = create_connection("wss://ws.kraken.com/")

for pair in pairs:
    pairname = pair.replace('/','_') 
    pd.DataFrame(columns=['ticker','best_bid','best_ask','time']).to_csv(f'feeds/{pairname}.csv',index=False)
    ws.send('{"event":"subscribe", "subscription":{"name":"ticker"}, "pair":["' + pair + '"]}')
    print(f'subscribed to {pair}')

while True:
    resp = ws.recv()
    if resp != '{"event":"heartbeat"}':
        try:
            response = parse_response(resp)
            response.update({'time':datetime.now()})
            filename = response['ticker'].replace('/','_')
            pd.DataFrame([response]).to_csv(f'feeds/{filename}.csv',index=False,header=False,mode='a')
            print(response)
        except KeyError:
            print(f'Invalid format: {resp}\n')
            pass
