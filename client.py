import socket

host_ip, server_port = "127.0.0.1", 9999
data = "XBT/USD BUY 1000\n" ### Enter desired request here ###

tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    tcp_client.connect((host_ip, server_port))
    tcp_client.sendall(data.encode())
    received = tcp_client.recv(1024)
finally:
    tcp_client.close()

print (f"Bytes Sent:     {data}")
print (f"Bytes Received: {data}")
print(received)