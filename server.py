import pandas as pd 
import socketserver

class Handler_TCPServer(socketserver.BaseRequestHandler):

    def handle(self):
        self.data = self.request.recv(1024).strip()
        print(f"\n{self.client_address[0]} sent:\n{self.data}\n")  
        try:
            output = str(self.calculate_price(self.data))
        except FileNotFoundError:
            output = 'File not found - have you formatted the request correctly?'
        self.request.sendall(output.encode())

    def parse_request(self,request):
        req = request.decode().split(' ')
        ticker = req[0]
        side = req[1]
        size = float(req[2])
        return {'ticker' : ticker, 'side' : side, 'size' : size}

    def calculate_price(self,request):
        req = self.parse_request(request)
        ticker_name = req['ticker'].replace('/','_')
        file = pd.read_csv(f'feeds/{ticker_name}.csv')
        if req['side'] == 'BUY':
            return req['size'] * float(file.sort_values('time',ascending=False).head(1).best_ask)
        elif req['side'] == 'SELL':
            return req['size'] * float(file.sort_values('time',ascending=False).head(1).best_bid)

if __name__ == "__main__":
    host = "localhost"
    port = 9999
    tcp_server = socketserver.TCPServer((host, port), Handler_TCPServer)
    print(f'listening on port {port}')
    tcp_server.serve_forever()




