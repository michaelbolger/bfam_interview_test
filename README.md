## BFAM Interview Exercise - Basic TCP Server

This is a simple TCP server implemented in Python3.

To make the task closer to a living example, I have opted to make this server connect to the [Kraken](https://www.kraken.com/) (a major cryptocurrency spot exchange) websocket and get live Cryptocurrency price feeds for different pairs. Further documentation on their websocket can be found [here](https://docs.kraken.com/websockets/).

The server is comprised of three scripts:

* `prices.py` pulls prices from the Kraken websocket based on a list of potential pairs and writes them to CSVs in the `feeds` folder (which is created if not already extant).

* `server.py` handles incoming client requests, reads the best bid/ask for that pair from the relevant file, and responds with a quote (calculated as `size * price`).

* `start.sh` is a one-line Bash script designed to run both Python scripts simultaneously, while allowing the user to cancel both processes at the same time using `CTRL + C` (otherwise the Websocket feed would keep running until the user logs out).

`client.py` has also been provided as a simple client to send messages and receive responses to verify that the server works as intended.

### Requirements

`Python 3.X` (written using 3.7.5)

`Pandas` (written using 0.25.3)

`Websocket` (written using 0.2.1)

### Usage

1. Put desired currency pairs in the `pairs` list in `prices.py`. `XBT/EUR` and `XBT/USD` have been provided as an example. The complete list of their available currency pairs can be seen [here](https://support.kraken.com/hc/en-us/articles/201893658-Currency-pairs-available-for-trading-on-Kraken).
2. Put relevant `host` and `port` in the variables of the same name in `server.py`. `localhost` and `9999` are provided as an example.
3. Run `bash start.sh`
4. To test that the server works correctly, `client.py` is provided. Put your request in the `data` variable and run `python client.py`, you should receive a numeric output which is your quote.

### Ideas for improvement

Due to time constraints, I have kept the server extremely simple, however if one were to flesh it out, some ideas could include:

* If using the current structure, only read the last (correctly-formatted) line from each CSV in order to keep it performant when the CSVs get large, e.g. if the server had been running for several days. In its current form the responses will become slower the longer it remains active

* A more robust structure would be to write the data to a database and pull it from there as needed. This may not be particularly performant in Python, however using KDB this would be a very good solution (I have not used KDB in some time which is why I opted for Python here)

* Unit testing to verify that the server is pulling the correct data and calculating the quotes correctly.

* Improving the user interface so different variables such as currency pairs or ports can be changed more easily.





